package com.kof.lock.core.redisSharedLock.lock.annotion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LockKeyElement {

    String element() ;
    /** 默认是-1 ，表示 不需要排序：对key的拼接 可以随意选择 ：确定的一种方式**/
    int elementOrder() default -1;
}
