package com.kof.lock.core.demo.service;

import com.kof.lock.core.demo.MyOrderDto;
import com.kof.lock.core.demo.OrderDto;
import com.kof.lock.core.redisSharedLock.lock.annotion.ExclusiveLock;
import com.kof.lock.core.redisSharedLock.lock.annotion.LockKeyElement;
import com.kof.lock.core.redisSharedLock.lock.constant.LockPolicy;
import org.springframework.stereotype.Service;

@Service
public class ExclusiveLockTestService {


    @ExclusiveLock(lockPrifex = "test")
    public void lock(OrderDto dto){

        System.out.println("lock方法开始,lockparams="+dto);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+dto);
    }


    @ExclusiveLock(lockPrifex = "test",policy = LockPolicy.do_nothing)
    public void lockWithDoNothingPolicy(@LockKeyElement(element = "order")String lockParams2){

        System.out.println("lock方法开始,lockparams="+lockParams2);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+lockParams2);
    }

    @ExclusiveLock(lockPrifex = "test",policy = LockPolicy.wait_and_retry,waitTime = 1000l,maxRetryTimes = 5)
    public void lockWaitAndRetry(@LockKeyElement(element = "key")String lockParams2){

        System.out.println("lock方法开始,lockparams="+lockParams2);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+lockParams2);
    }
    @ExclusiveLock(lockPrifex = "test2")
    public void lock2(MyOrderDto myOrderDto) {

        System.out.println("lock方法开始,lockparams="+myOrderDto);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+myOrderDto);
    }
}
