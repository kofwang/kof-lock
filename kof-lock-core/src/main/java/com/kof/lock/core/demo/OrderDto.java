package com.kof.lock.core.demo;

import com.kof.lock.core.redisSharedLock.lock.annotion.LockKeyElement;

import java.io.Serializable;

public class OrderDto implements Serializable {

    @LockKeyElement(element = "orderName")
    private String orderName;
    @LockKeyElement(element = "orderId")
    private String orderNum;

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }
}
