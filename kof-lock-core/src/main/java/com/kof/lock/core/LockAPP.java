package com.kof.lock.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LockAPP {

    public static void main(String[] args){
        SpringApplication.run(LockAPP.class, args);
    }
}
