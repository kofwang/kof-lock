package com.kof.lock.core.redisSharedLock.lock.annotion;

import com.kof.lock.core.redisSharedLock.lock.constant.LockPolicy;

public @interface ExclusiveLock {
    /** 锁 **/
    String lockPrifex() default "";

    /** 策略 */
    LockPolicy policy() default LockPolicy.throw_exception;

    /** 最大持有锁时间*/
    long maxLockTime() default 300000l;

    /**
     * LockPolicy为wait_and_retry模式时的等待重试时间,单位是毫秒
     */
    long waitTime() default 500l;

    /**
     * LockPolicy为wait_and_retry模式时的重试次数
     * @return
     */
    int maxRetryTimes() default 10;
}
