package com.kof.lock.core.demo;

import java.io.Serializable;

public class MyOrderDto implements Serializable {

    private Headers headers;
    private OrderDto orderDto;

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public OrderDto getOrderDto() {
        return orderDto;
    }

    public void setOrderDto(OrderDto orderDto) {
        this.orderDto = orderDto;
    }
}
