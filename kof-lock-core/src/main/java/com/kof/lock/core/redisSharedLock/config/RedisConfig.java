package com.kof.lock.core.redisSharedLock.config;


import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;
import java.io.Serializable;

@Configuration
@EnableAutoConfiguration
public class RedisConfig {

	public static Logger logger = LoggerFactory.getLogger(RedisConfig.class);

	@Resource
	private LettuceConnectionFactory lettuceConnectionFactory;


	@Bean
	public RedisTemplate<String, Serializable> redisTemplate() {
        RedisTemplate<String, Serializable> template = new RedisTemplate<>();
        /** todo 修改序列化方式 ：：：**/
	    template.setHashKeySerializer(new StringRedisSerializer());
	    template.setHashValueSerializer(new FastJsonRedisSerializer(Object.class));
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		template.setConnectionFactory(lettuceConnectionFactory);
		return template;
    }


	@Bean
	@ConfigurationProperties(prefix = "spring.redis")
	public CacheManager cacheManager(LettuceConnectionFactory factory) {
		RedisCacheManager cacheManager = RedisCacheManager.create(factory);
		return cacheManager;
	}

}
