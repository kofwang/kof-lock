package com.kof.lock.core.demo.service;


import com.kof.lock.core.redisSharedLock.lock.annotion.LockKeyElement;
import com.kof.lock.core.redisSharedLock.lock.annotion.SharedLock;
import com.kof.lock.core.redisSharedLock.lock.constant.LockPolicy;
import org.springframework.stereotype.Service;

@Service
public class SharedLockTestService {

    @SharedLock(lockPrifex = "test")
    public void lock(@LockKeyElement(element = "key") String lockParams){

        System.out.println("lock方法开始,lockparams="+lockParams);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+lockParams);
    }


    @SharedLock(lockPrifex = "test",policy = LockPolicy.do_nothing)
    public void lockWithDoNothingPolicy(@LockKeyElement(element = "key")String lockParams2){

        System.out.println("lock方法开始,lockparams="+lockParams2);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+lockParams2);
    }

    @SharedLock(lockPrifex = "test",policy = LockPolicy.wait_and_retry,waitTime = 1000l,maxRetryTimes = 5)
    public void lockWaitAndRetry(@LockKeyElement(element = "key")String lockParams2){

        System.out.println("lock方法开始,lockparams="+lockParams2);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("lock方法结束,lockparams="+lockParams2);
    }

}
