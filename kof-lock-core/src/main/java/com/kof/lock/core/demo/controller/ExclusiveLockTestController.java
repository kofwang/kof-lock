package com.kof.lock.core.demo.controller;

import com.kof.lock.core.demo.Headers;
import com.kof.lock.core.demo.MyOrderDto;
import com.kof.lock.core.demo.OrderDto;
import com.kof.lock.core.demo.service.ExclusiveLockTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 @RestController
@RequestMapping("exclusiveLockTest")
 public class ExclusiveLockTestController{

    @Autowired
    private ExclusiveLockTestService exclusiveLockTestService;

    @RequestMapping("11")
    public String testLock(String lockParam ,Integer id) {

        OrderDto dto=new OrderDto();
        dto.setOrderName(lockParam);
        dto.setOrderNum(id.toString());
        try {
            exclusiveLockTestService.lock(dto);
            return "success";
        } catch (Exception e) {
            return "error:" + lockParam + "has been locked";
        }
    }

     @RequestMapping("12")
     public String testLock2(String lockParam ,Integer id) {

         OrderDto dto=new OrderDto();
         dto.setOrderName(lockParam);
         dto.setOrderNum(id.toString());
         Headers headers=new Headers();
         headers.setCode("123123");
         headers.setMsg("12312313123");
         MyOrderDto myOrderDto =new MyOrderDto();
         myOrderDto.setHeaders(headers);
         myOrderDto.setOrderDto(dto);

         try {
             exclusiveLockTestService.lock2(myOrderDto);
             return "success";
         } catch (Exception e) {
             return "error:" + lockParam + "has been locked";
         }
     }

    @RequestMapping("2")
    public String testLock2(String lockParam) {
        exclusiveLockTestService.lockWithDoNothingPolicy(lockParam);
        return "success";

    }


    @RequestMapping("3")
    public String testLock3(String lockParam) {
        exclusiveLockTestService.lockWaitAndRetry(lockParam);
        return "success";

    }

}